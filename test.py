# -*- coding: utf-8 -*-

'''
Program to request Sentinel 2 images massively from Copernicus Open Access Hub.

Author: Ausiàs R. T.
'''

# %% Libraries
# Built-in
# Built-in
import xml.etree.ElementTree as ET
import math as m
import time
from tqdm import tqdm
import subprocess
import pathlib
import os
import sys
import pickle

from sentinelsat import SentinelAPI

# User interface
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QTextEdit, QFileDialog, QToolButton, QLabel, QCheckBox, QDateEdit, QDialog, QTableWidgetItem, QTableWidget
from PyQt5 import uic, QtCore, QtWidgets
from PyQt5 import QtTest
from tkinter.filedialog import askdirectory

import easygui
# %%

# User interface
class UI(QMainWindow):
	def __init__(self):
		super(UI, self).__init__()
		# Run main window
		uic.loadUi("./main.ui", self)
		
		# Open config path
		self.openconfigbutton.clicked.connect(self.open_config)
		
		# Load products file
		self.selectfilebutton.clicked.connect(self.load_product_file)
		
		# Request imatges
		self.requestbutton.clicked.connect(self.request_images)
		
		# Request imatges
		self.downloadbutton.clicked.connect(self.download_from_file)
		
		# Show everything
		self.show()
	
	# Utilities
	def request_uuids(self):
		QApplication.processEvents()
		self.output.setText("Starting...")
		self.status.setText("Starting...")
		try:
			# Search for parameters
			with open("parameters.txt", 'r') as f:
				for index, line in enumerate(f):
					# Search string
					if 'USER' in line:
						user = line.split('"')[1]
					if 'PASSWORD' in line:
						password = line.split('"')[1]
			
			# Autentication
			self.api = SentinelAPI(str(user), str(password))
		except:
			self.output.append("Error with user autentication")
		
		# Parse data products
		if self.products.split('.')[-1] == 'xml' or self.products.split('.')[-1] == 'meta4':
			with open(self.products, 'r') as f:
				data = f.read()
			
			root = ET.fromstring(data)
			product_names = list()
			
			# Get product names
			for child in root:
				product_names.append(child.attrib['name'])
		
		# For pickled Sentinel file names
		if self.products.split('.')[-1] == 'pkl':
			with open(self.products, 'rb') as f:
				product_names = pickle.load(f)
		
		# Retrieve availability
		n_products = len(product_names)
		product_uuid = list()
		availability = list()
		failed = list()
		
		# Format progress bar
		self.progressbar.setMaximum(n_products)
		self.progressbar.setMinimum(0)
		self.progressbar.setValue(0)
		
		self.status.setText("Getting file identifier...")
		self.output.append("Getting file identifier...")
		# Getting file identifier
		for i in range(n_products):
			QApplication.processEvents()
			try:
				uuid = list(self.api.query(identifier=product_names[i]).keys())[0]
				product_uuid.append(uuid)
				availability.append(self.api.is_online(uuid))
			except:
				self.output.append('Requesting image nº:' + str(i) + ' with name '+product_names[i]+' had failed.')
				failed.append(product_names[i])
			self.progressbar.setValue(i+1)
		return product_uuid, availability, n_products, failed
	
	def open_config(self):
		path = str(pathlib.Path().resolve()).replace('\\','/')+'/'
		os.system('cd "'+path+'" & start .')
	
	def load_product_file(self):
		self.products = easygui.fileopenbox(title='Select the products file', filetypes=["*.meta4"])
		self.fileselector.setText(self.products)
	
	def request_images(self):
		product_uuid, availability, n_products, failed = self.request_uuids()
		
		# Save image uuids
		# Get workpath
		workpath = self.products.split(self.products.split('\\')[-1])[0].replace('\\', '/')
		url_template = "https://scihub.copernicus.eu/dhus/odata/v1/Products('$$')/$value"
		urls = [url_template.replace("$$", uuid) for uuid in product_uuid]
		with open(workpath+"uuids.txt", "w") as output:
			for url in urls:
				output.write(url+'\n')
		output.close()
		
		# Finished
		self.output.append("The uuid request had finished.")
		self.status.setText("Finished")
		
		#######################################################################
		# Activate products
		# Filter and drop online products
		offline = [x for x, y in zip(product_uuid, availability) if y == False]

		# Request offline products 20 by 20 (maximum allowed)
		maxp = 19
		n_offline = len(offline)
		n_packs = m.ceil(n_offline/maxp)
		j = 0
		# For bigger requests
		if n_offline > maxp:
			# Information
			self.output.append(str(len(offline)) + " images left.")
			self.progressbar.setMaximum(len(offline))
			self.progressbar.setMinimum(0)
			self.progressbar.setValue(0)
			c = 0
			self.status.setText("Working...")
			# Split data in a smaller segment
			segment = offline[0:maxp]
			# First segment
			for request in segment:
				self.output.append('Request: ' + request)
				# Retry if the server fails
				try:
					self.api.trigger_offline_retrieval(request)
				except:
					self.output.append('Retry: ' + request)
					self.api.trigger_offline_retrieval(request)
				QtTest.QTest.qWait(10*1000)
			
			# Once these have been requested, retrieve the online ones
			self.output.append('Waiting...')
			QtTest.QTest.qWait(10*60*1000)
			j = maxp + 1
			while j < n_offline -1:
				# Information
				self.output.append(str(len(segment)) + " images left.")
				# Check if there is any request online
				for request in segment:
					result = self.api.is_online(request)
					if result is True:
						self.output.append('Image available:' + request)
						# Remove the finished one
						segment.remove(request)
						# Process the next offline product
						self.output.append('Request: ' + offline[j])
						# Retry if the server fails
						try:
							self.api.trigger_offline_retrieval(offline[j])
						except:
							self.output.append('Retry: ' + offline[j])
							self.api.trigger_offline_retrieval(offline[j])
						segment.append(offline[j])
						j = j + 1
						c = c + 1 # Pot ser ací s'haja de pujar algun nivell
				self.output.append('Waiting...')
				QtTest.QTest.qWait(10*60*1000)
				
				self.progressbar.setValue(c)
			
		# For smaller requests:
		else:
			self.progressbar.setMaximum(len(offline))
			self.progressbar.setMinimum(0)
			self.progressbar.setValue(0)
			self.status.setText('Waiting...')
			self.output.append('Activating products...')
			c = 0
			for request in offline:
				QApplication.processEvents()
				self.output.append('Request: ' + request)
				# Retry if the server fails
				try:
					self.api.trigger_offline_retrieval(request)
				except:
					self.output.append('Retry: ' + request)
					self.api.trigger_offline_retrieval(request)
				c = c + 1
				self.progressbar.setValue(c)
				QtTest.QTest.qWait(60*1000)
			
			# Wait
			QApplication.processEvents()
			self.output.append('Waiting for activation...')
			
			self.progressbar.setMaximum(len(offline))
			self.progressbar.setMinimum(0)
			self.progressbar.setValue(0)
			self.status.setText('Waiting...')
			self.output.append('Checking activation...')
			c = 0
			
			while len(offline) >= 1:
				QApplication.processEvents()
				# Information
				self.output.append(str(len(offline)) + " images left.")
				for request in offline:
					result = self.api.is_online(request)
					if result is True:
						self.output.append('Image available:' + request)
						# Remove the finished one
						try:
							offline.remove(request)
							c = c + 1
						except:
							break
				self.output.append('Waiting...')
				
				self.progressbar.setValue(c)
				QtTest.QTest.qWait(10*60*1000)
			self.output.append('Finished')
		self.status.setText('Process completed...')
		QApplication.processEvents()
	
	###########################################################################
	def download_from_file(self):
		# Get uuids
		product_uuid, availability, n_products, failed = self.request_uuids()
		
		download_path = self.products.split(self.products.split('\\')[-1])[0].replace('\\', '/')
		# Download files
		self.progressbar.setMaximum(len(product_uuid))
		self.progressbar.setMinimum(0)
		self.progressbar.setValue(0)
		c = 0
		self.status.setText('Downloading...')
		self.output.append('Downloading (the program might freeze, do not touch it)')
		for uuid in product_uuid:
			self.output.append('Downloading: '+uuid)
			QApplication.processEvents()
			self.api.download(uuid, directory_path=download_path)
			c += 1
			QApplication.processEvents()
			self.progressbar.setValue(c)
		
		self.output.append('Finished')
		self.status.setText('Process completed...')
		
# Run and show UI
app = QApplication(sys.argv)
UIWindow = UI()

app.exec_()

